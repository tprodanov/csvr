CXX      := g++
CXXFLAGS := -O2 -std=c++11 -pedantic-errors -Wall -Wextra -Werror
BIN_DIR  := bin
TARGET   := csvr
SRC_DIR  := src
SRC      := $(SRC_DIR)/$(TARGET).cpp
PREFIX   := /usr/local
SCRIPTS  := cl

all: $(BIN_DIR)/$(TARGET)

$(BIN_DIR)/$(TARGET): $(SRC)
	@mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) -o $(BIN_DIR)/$(TARGET) $^

.PHONY: all clean install uninstall

clean:
	-@rm -rvf $(BIN_DIR)/*

install: $(BIN_DIR)/$(TARGET) $(SCRIPTS)
	mkdir -p $(PREFIX)/bin
	$(foreach f, $^, cp $(f) $(PREFIX)/bin/$(notdir $(f));)

uninstall:
	$(foreach f, $(TARGET) $(SCRIPTS), rm -f $(PREFIX)/bin/$(f);)
