#include<iostream>
#include<iomanip>
#include<vector>
#include<sstream>
#include<string>
#include<fstream>
#include<functional>

using line_t = std::vector<std::string>;
// vector of pairs (bool is_comment, line_t splitted_line).
using lines_t = std::vector<std::pair<bool, line_t>>;
using widths_t = std::vector<size_t>;

enum align {
    left,
    right,
    automatic,
};

struct arguments {
    std::string filename = "";
    size_t buffer = 1000;
    std::string delimiter = "\t";
    size_t width = 30;
    bool cut = true;
    bool skip_header = false;
    std::string comment = "#";
    bool skip_comments = false;
    bool col_nums_from_1 = true;
    align align_side = align::left;
};

line_t split(std::string const& str, std::string const& delimiter) {
    line_t res;
    size_t start = 0;
    size_t pos = 0;
    while ((pos = str.find(delimiter, start)) != std::string::npos) {
        res.push_back(str.substr(start, pos - start));
        start = pos + delimiter.size();
    }
    res.push_back(str.substr(start));
    return res;
}

void calculate_widths(lines_t const& lines, widths_t& widths, size_t max_width) {
    widths.clear();
    for (auto const& pair : lines) {
        if (pair.first) {
            continue;
        }

        line_t const& line = pair.second;
        if (line.size() > widths.size()) {
            widths.resize(line.size(), 0);
        }
        for (size_t j = 0; j < line.size(); ++j) {
            widths[j] = std::max(widths[j], line[j].size());
        }
    }

    for (size_t j = 0; j < widths.size(); ++j) {
        widths[j] = std::min(widths[j], max_width);
    }
}

bool read_next(std::istream& inp, lines_t& lines, arguments const& args) {
    lines.clear();
    std::string line;
    for (size_t i = 0; i < args.buffer; ++i) {
        if (!inp) {
            if (!lines.empty()) {
                line_t const& last = lines[lines.size() - 1].second;
                if (last.empty() || (last.size() == 1 && last[0].empty())) {
                    lines.pop_back();
                }
            }
            return false;
        }

        std::getline(inp, line);
        // starts_with
        if (!args.comment.empty() && line.rfind(args.comment, 0) == 0) {
            line_t tmp_vec;
            if (!args.skip_comments) {
                tmp_vec.push_back(line);
                lines.push_back(std::make_pair(true, tmp_vec));
            }
            continue;
        }

        line_t splitted = split(line, args.delimiter);
        lines.push_back(std::make_pair(false, splitted));
    }
    return true;
}

bool align_left(std::string const& str, align align_side) {
    switch (align_side) {
    case align::left:
        return true;
    case align::right:
        return false;
    case align::automatic:
        // If all numbers, align to the right, otherwise to the left.
        return str.find_first_not_of("0123456789.-") != std::string::npos;
    }
    std::cerr << "Unreachable statement\n";
    exit(1);
}

void write(std::ostream& outp, lines_t const& lines, widths_t const& widths, arguments const& args) {
    for (auto const& pair : lines) {
        line_t const& line = pair.second;
        if (pair.first) {
            outp << line[0] << '\n';
            continue;
        }

        for (size_t j = 0; j < line.size(); ++j) {
            if (j) {
                outp << "  ";
            }
            if (args.cut && line[j].size() > widths[j]) {
                outp << line[j].substr(0, widths[j] - 1) << '$';
            } else {
                if (align_left(line[j], args.align_side)) {
                    outp << std::left << std::setfill(' ') << std::setw(widths[j]) << line[j];
                } else {
                    outp << std::right << std::setfill(' ') << std::setw(widths[j]) << line[j];
                }
            }
        }
        outp << '\n';
    }
}

void process(std::istream& inp, std::ostream& outp, arguments const& args) {
    lines_t lines;
    widths_t widths;
    bool file_continues = true;
    bool need_header = !args.skip_header;

    while (file_continues) {
        file_continues = read_next(inp, lines, args);
        calculate_widths(lines, widths, args.width);

        if (need_header) {
            for (size_t j = 0; j < widths.size(); ++j) {
                if (j) {
                    outp << "  ";
                }
                std::string col_str = std::to_string(j + args.col_nums_from_1);
                widths[j] = std::max(widths[j], col_str.size());
                outp << std::left << std::setfill(' ') << std::setw(widths[j]) << col_str;
            }
            outp << '\n';
            need_header = false;
        }

        write(outp, lines, widths, args);
    }
}

/// Replace "\\t" to "\t" in a string.
void replace_tabs(std::string &str) {
    size_t pos = 0;
    while ((pos = str.find("\\t")) != std::string::npos) {
        str.replace(pos, 2, "\t");
    }
}

void parse_args(size_t argc, char* argv[], arguments& args) {
    std::stringstream usage;
    std::string prog = argv[0];
    prog = prog.substr(prog.find_last_of('/') + 1);
    usage << "usage: " << prog << " [FILE] [args]\n\n"
          << "Input arguments:\n"
          << "    FILE                     Input CSV file [stdin].\n\n"
          << "Optional arguments:\n"
          << "    -d STR, --delimiter STR  Delimiter [\\t].\n"
          << "    -w INT, --width INT      Maximal column width [" << args.width << "].\n"
          << "    -k, --keep               Do not cut columns longer than -w INT.\n"
          << "    -a STR, --align STR      Column alignment argument [left].\n"
          << "                             Possible choices: left (l), right(r), auto(a).\n"
          << "    --skip-header            Do not write column numbers in the header.\n"
          << "    -0                       Start column numbers from zero.\n"
          << "    -c STR, --comment STR    Do not split comment lines that start with STR [" << args.comment << "].\n"
          << "                             Use empty string to use all lines.\n"
          << "    --skip-comments          Do not write comments at all.\n"
          << "    -b INT, --buffer INT     Buffer size [" << args.buffer << "].\n\n"
          << "Other arguments:\n"
          << "    -h, --help               Show this message and exit\n";

    std::vector<std::string> sargv;
    for (size_t i = 1; i < argc; ++i) {
        sargv.push_back(argv[i]);
    }

    for (size_t i = 0; i < argc - 1; ++i) {
        std::string const& arg = sargv[i];
        if (arg == "-h" || arg == "--help") {
            std::cout << usage.str();
            exit(0);
        }
        else if (arg == "-b" || arg == "--buffer") {
            args.buffer = std::stoi(sargv[++i]);
        }

        else if (arg == "-d" || arg == "--delimiter") {
            args.delimiter = sargv[i + 1];
            replace_tabs(args.delimiter);
            ++i;
        } else if (arg == "-c" || arg == "--comment") {
            args.comment = sargv[i + 1];
            replace_tabs(args.comment);
            ++i;
        }

        else if (arg == "-w" || arg == "--width") {
            args.width = std::stoi(sargv[++i]);
        }
        else if (arg == "-k" || arg == "--keep") {
            args.cut = false;
        } else if (arg == "--skip-comments") {
            args.skip_comments = true;
        } else if (arg == "--skip-header") {
            args.skip_header = true;
        } else if (arg == "-0") {
            args.col_nums_from_1 = false;
        }

        else if (arg == "-a" || arg == "--align") {
            std::string const& side = sargv[i + 1];
            if (side == "l" || side == "left") {
                args.align_side = align::left;
            } else if (side == "r" || side == "right") {
                args.align_side = align::right;
            } else if (side == "a" || side == "auto") {
                args.align_side = align::automatic;
            } else {
                std::cerr << "Invalid --align argument: \"" << side
                    << "\", possible choices: left (l), right (r), auto (a)." << std::endl;
                exit(1);
            }
            ++i;
        }

        else if (arg[0] == '-' || args.filename.size()) {
            std::cerr << "Invalid argument: " << arg << "\n\n";
            std::cerr << usage.str() << std::endl;
            exit(1);
        }
        else {
            args.filename = arg;
        }
    }
}

int main(int argc, char* argv[]) {
    arguments args;
    parse_args(static_cast<size_t>(argc), argv, args);
    if (args.filename.size()) {
        std::ifstream f(args.filename);
        if (!f.good()) {
            std::cerr << "Cannot open file \"" << args.filename << "\"." << std::endl;
            exit(1);
        }
        process(f, std::cout, args);
    } else {
        process(std::cin, std::cout, args);
    }
    return 0;
}
